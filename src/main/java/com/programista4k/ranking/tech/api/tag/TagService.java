package com.programista4k.ranking.tech.api.tag;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TagService {

    private final TagRepository repo;

    public List<Tag> getAll() {

        return repo.findAll();
    }

    public List<Tag> getAllByNames(List<String> tagNames) {
        return getAll().stream()
                .filter(t -> tagNames.contains(t.getName()))
                .collect(Collectors.toList());
    }

}
