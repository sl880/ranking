package com.programista4k.ranking.tech.api.country;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="v_countries")
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class Country {

    @Id
    @Column(name="iso_code")
    private String isoCode;

    @Column(name="name")
    private String name;

}
