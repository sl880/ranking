package com.programista4k.ranking.tech.api.ranking.filter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.lang.NonNull;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class CountryFilter {

    @NotNull
    private String isoCode;

}
