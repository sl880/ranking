package com.programista4k.ranking.tech.api.ranking.filter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.jetbrains.annotations.NotNull;


@NoArgsConstructor
@AllArgsConstructor
@Getter
public class TagFilter {

    @NotNull
    private String name;
}
