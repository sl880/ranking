package com.programista4k.ranking.tech.api.ranking;

import com.programista4k.ranking.tech.api.country.Country;
import com.programista4k.ranking.tech.api.tag.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Repository
interface RankingRepository extends JpaRepository<Ranking, Ranking> {

    @Query("SELECT new com.programista4k.ranking.tech.api.ranking.Ranking(r.date, r.tag, sum(r.score)) " +
            "FROM v_ranking r " +
            "WHERE r.tag in (:tags) " +
            "GROUP BY r.date, r.tag " +
            "ORDER BY r.tag, r.date")
    public List<Ranking> findAllAggByTags(@PathVariable List<Tag> tags);

    @Query("SELECT new com.programista4k.ranking.tech.api.ranking.Ranking(r.date, r.country, r.tag, sum(r.score)) " +
            "FROM v_ranking r " +
            "WHERE r.tag in (:tags) and r.country in (:countries) " +
            "GROUP BY r.date, r.country, r.tag " +
            "ORDER BY r.country, r.tag, r.date")
    List<Ranking> findAllByCountriesAndTags(@PathVariable("countries") List<Country> countries,
                                            @PathVariable("tags") List<Tag> tags);

}

