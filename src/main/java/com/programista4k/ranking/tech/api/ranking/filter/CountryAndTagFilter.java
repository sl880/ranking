package com.programista4k.ranking.tech.api.ranking.filter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class CountryAndTagFilter {
    
    @NotNull
    private CountryFilter countryFilter;

    @NotNull
    private TagFilter tagFilter;

}
