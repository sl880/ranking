package com.programista4k.ranking.tech.api.country;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor=@__(@Autowired))
public class CountryService {

    private final CountryRepository repo;

    public List<Country> getAllCountries() {
        return repo.findAll();
    }

    public List<Country> getAllCountriesByIsoCodes(List<String> isoCodes) {
        return repo.findAllById(isoCodes);
    }

}
