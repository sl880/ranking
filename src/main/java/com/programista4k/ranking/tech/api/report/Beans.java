package com.programista4k.ranking.tech.api.report;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Clock;

@Configuration
class Beans {

    @Bean
    public Clock clock() {
        return Clock.systemDefaultZone();
    }

    @Bean
    public String fileDir(@Value("${api.v1.report.dir}") String f) {
        return f;
    }

}
