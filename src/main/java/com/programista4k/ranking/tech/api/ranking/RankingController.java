package com.programista4k.ranking.tech.api.ranking;

import com.programista4k.ranking.tech.api.ranking.filter.CountryAndTagFilter;
import com.programista4k.ranking.tech.api.ranking.filter.TagFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor(onConstructor=@__(@Autowired))
@RequestMapping("/api/v1")
public class RankingController {

    private final RankingService service;

    @PostMapping("/ranking/agg")
    public List<Ranking> postRequestForAggRankingsByTags(@RequestBody List<TagFilter> tagFilters) {
        return service.getAllAggByTags(tagFilters);
    }

    @PostMapping("/ranking")
    public List<Ranking> postRequestForRankingsByCountriesAndTags(@RequestBody List<CountryAndTagFilter> countryAndTagFilters) {
        return service.getAllByCountriesAndTags(countryAndTagFilters);
    }

}
