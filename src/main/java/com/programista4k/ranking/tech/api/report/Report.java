package com.programista4k.ranking.tech.api.report;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
class Report {

    @NotNull
    private String type;

    private String head;

    private String body;

}
