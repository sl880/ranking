package com.programista4k.ranking.tech.api.report;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor(onConstructor=@__(@Autowired))
@RequestMapping("/api/v1")
public class ReportController {

    private final ReportService service;

    @PostMapping("/report")
    public HttpStatus sendReport(@RequestBody Report report) {
        boolean isSaved = service.saveReport(report);
        return isSaved ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR;
    }

}
