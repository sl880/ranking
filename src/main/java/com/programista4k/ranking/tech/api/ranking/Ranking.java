package com.programista4k.ranking.tech.api.ranking;

import com.programista4k.ranking.tech.api.country.Country;
import com.programista4k.ranking.tech.api.tag.Tag;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity(name="v_ranking")
@NoArgsConstructor
@AllArgsConstructor
@Getter
class Ranking {

    @Id
    private Long id;

    private LocalDateTime date;

    @OneToOne
    @JoinColumn(name = "country_iso_code", referencedColumnName = "iso_code")
    private Country country;

    @OneToOne
    @JoinColumn(name = "tag_id", referencedColumnName = "id")
    private Tag tag;

    @Column(name="count")
    private Long score;

    public Ranking(LocalDateTime date, Country country, Tag tag, Long count) {
        this.date = date;
        this.country = country;
        this.tag = tag;
        this.score = count;
    }

    public Ranking(LocalDateTime date, Tag tag, Long count) {
        this.date = date;
        this.tag = tag;
        this.score = count;
    }
}
