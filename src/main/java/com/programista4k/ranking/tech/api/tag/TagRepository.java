package com.programista4k.ranking.tech.api.tag;

import org.springframework.data.jpa.repository.JpaRepository;

interface TagRepository extends JpaRepository<Tag, Integer> {
}
