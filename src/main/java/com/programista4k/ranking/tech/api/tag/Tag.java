package com.programista4k.ranking.tech.api.tag;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="v_tags")
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class Tag {

    @Id
    private Integer id;

    private String name;

}
