package com.programista4k.ranking.tech.api.ranking;

import com.programista4k.ranking.tech.api.country.Country;
import com.programista4k.ranking.tech.api.country.CountryService;
import com.programista4k.ranking.tech.api.ranking.filter.CountryFilter;
import com.programista4k.ranking.tech.api.ranking.filter.CountryAndTagFilter;
import com.programista4k.ranking.tech.api.ranking.filter.TagFilter;
import com.programista4k.ranking.tech.api.tag.Tag;
import com.programista4k.ranking.tech.api.tag.TagService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor=@__(@Autowired))
class RankingService {

    private final RankingRepository repo;
    private final TagService tagService;
    private final CountryService countryService;

    public List<Ranking> getAllByCountriesAndTags(List<CountryAndTagFilter> filters) {

        List<String> isoCodes = filters.stream()
                .map(CountryAndTagFilter::getCountryFilter)
                .map(CountryFilter::getIsoCode)
                .collect(Collectors.toList());
        List<Country> countries = countryService.getAllCountriesByIsoCodes(isoCodes);

        List<String> tagNames = filters.stream()
                .map(CountryAndTagFilter::getTagFilter)
                .map(TagFilter::getName)
                .collect(Collectors.toList());
        List<Tag> tags = tagService.getAllByNames(tagNames);

        return repo.findAllByCountriesAndTags(countries, tags);
    }

    public List<Ranking> getAllAggByTags(List<TagFilter> filters) {
        List<String> tagsNames = filters.stream().map(TagFilter::getName).collect(Collectors.toList());
        List<Tag> tags = tagService.getAll().stream()
                .filter(t -> tagsNames.contains(t.getName()))
                .collect(Collectors.toList());
        return repo.findAllAggByTags(tags);
    }

}
