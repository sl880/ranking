package com.programista4k.ranking.tech.api.report;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.Clock;
import java.util.StringJoiner;

@Service
@Log4j2
@RequiredArgsConstructor
class ReportService {

    @Value("${api.v1.report.dir}")
    private final String fileDir;

    private final String FILE_HEAD = "_report_";
    private final String FILE_FOOTER = ".txt";
    private final Clock clock;

    public boolean saveReport(Report report) {
        try (FileWriter fw = new FileWriter(getFileName(report));
             PrintWriter pw = new PrintWriter(fw);
        ) {
            StringJoiner sj = new StringJoiner("\n");
            String s = sj.add(report.getHead()).add(report.getBody()).toString();
            pw.println(s);
        } catch (IOException e) {
            log.error("Saving report failed.", e);
            return false;
        }
        return true;
    }

    private String getFileName(Report report) {
        StringJoiner sj = new StringJoiner("");
        return sj.add(fileDir)
                .add(report.getType())
                .add(FILE_HEAD)
                .add(clock.instant().toString())
                .add(FILE_FOOTER)
                .toString();
    }

}
