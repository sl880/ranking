package com.programista4k.ranking.tech.api;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class TechRankingApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechRankingApiApplication.class, args);
	}

	@Bean
	public CommandLineRunner setUp() {
		return args -> {
			//List<Ranking> c = r.findAll();
			//System.out.println(c);
		};
	}
}
